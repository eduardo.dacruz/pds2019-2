<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('home');
Route::resource('event', 'EventController');
Route::resource('comment', 'ComentarioController');

Route::post('event/search', 'EventController@search')->name('event.search');
Route::get('event/details/{event}', 'EventController@details')->name('event.details');

Route::resource('establishment', 'EstablishmentController');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
