<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Laravel\Scout\Searchable;

class Event extends Model
{
    use Searchable;

        public function searchableAs()
            {
                return 'events_index';
            }

            public function comments()
            {
                return $this->hasMany('App\Comentario');
            }
    //
}
