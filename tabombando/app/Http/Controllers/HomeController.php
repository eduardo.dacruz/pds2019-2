<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Event;
use App\Establishment;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $events = Event::whereNotNull('lat')->orderBy('value', 'desc')->get();
        $establishments = Establishment::whereNotNull('lat')->orderBy('value', 'desc')->get();
        $all = $events->toBase()->merge($establishments)->sortByDesc('value');
        return view('home')->with('markersJson', $all->toJson())->with('markers', $all);
    }
}
