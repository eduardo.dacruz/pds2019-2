<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Event;
use App\Establishment;

class EventController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    $events = Event::whereNotNull('lat')->orderBy('value', 'desc')->get();
            return view('event')->with('markersJson', $events->toJson())->with('markers', $events);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $event = new Event;
        $event->name = $request->nameEvent;
        $dtBegin = $request->dtBegin;
        $dtEnd = $request->dtEnd;
        $event->start = $request->hrBegin;
        $event->end = $request->hrEnd;
        $event->lat = $request->address_latitude;
        $event->lng = $request->address_longitude;
        $event->address = $request->address;

        $event->save();

        return redirect()->route('home');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $event = Event::find($id);
        if($event == null)
            $event = Establishment::find($id);
        $event->value++;
        $event->save();

        return redirect()->route('home');
    }

    public function details($id)
    {
        $event = Event::find($id);
        if($event == null)
            $event = Establishment::find($id);
        $event->value++;
        $event->save();

        $events = Event::whereNotNull('lat')->orderBy('value', 'desc')->get();
        $establishments = Establishment::whereNotNull('lat')->orderBy('value', 'desc')->get();
        $all = $events->toBase()->merge($establishments)->sortByDesc('value');
        return view('home')->with('markersJson', $all->toJson())->with('markers', $all)->with('event', $event);
    }


    public function search(Request $search)
        {
            $events = Event::search($search->search)->get();
            return view('home')->with('markersJson', $events->toJson())->with('markers', $events);
        }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
