<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Establishment;
use App\Event;

class EstablishmentController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $events = Event::whereNotNull('lat')->orderBy('value', 'desc')->get();
        $establishments = Establishment::whereNotNull('lat')->orderBy('value', 'desc')->get();
        $all = $events->toBase()->merge($establishments)->sortByDesc('value');
        return view('establishment')->with('markersJson', $all->toJson())->with('markers', $all);
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $establishment = new Establishment;
        $establishment->name = $request->nameEst;
        $establishment->lat = $request->address_latitude;
        $establishment->lng = $request->address_longitude;
        $establishment->address = $request->address;
        $establishment->start = $request->hrOpen;
        $establishment->end = $request->hrClose;


        $establishment->save();

        return redirect()->route('home');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $establishment = Establishment::find($id);
        $establishment->value++;
        $establishment->save();

        return redirect()->route('home');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
