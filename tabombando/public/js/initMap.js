let map;
// global array to store the marker object 
let markersArray = [];

function initMap() { 
    console.log(markersArray.length);

    const sorocaba = {lat: -23.501414, lng: -47.452705};

    map = new google.maps.Map(document.getElementById('address-map'), {
        center: sorocaba,
        zoom: 13
    });

    for(var i in markers){
        let color = "yellow";
        if(markers[i].value > 20)
          color = "red";
        else if(markers[i].value > 10)
          color = "orange";
        addMarker({lat: markers[i].lat, lng: markers[i].lng}, color, markers[i].id);
    }
    
}

function addMarker(latLng, color, id) {
    let url = "http://maps.google.com/mapfiles/ms/icons/";
    url += color + "-dot.png";
  
    let marker = new google.maps.Marker({
      map: map,
      position: latLng,
      icon: {
        url: url
      }
    });

    marker.addListener('click', function(){
      console.log("Clicou no " + id);

      let url = "/event/details/:id";
      url = url.replace(':id', id);
      document.location.href=url;
    });
  
    //store the marker object drawn in global array
    markersArray.push(marker);
  }
