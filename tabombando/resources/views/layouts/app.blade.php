<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'TaBombando!') }}</title>

    <!-- <link rel= "stylesheet" href= "https://maxcdn.Bootstrapcdn.com/Bootstrap/3.3.2/css/Bootstrap.min.css" > -->
    <!-- <link rel= "stylesheet" href= "https://maxcdn.Bootstrapcdn.com/Bootstrap/3.3.2/css/Bootstrap-theme.min.css" > -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <script defer src="https://use.fontawesome.com/releases/v5.0.13/js/solid.js" integrity="sha384-tzzSw1/Vo+0N5UhStP3bvwWPq+uvzCMfrN1fEFe+xBmv1C/AtVX5K0uZtmcHitFZ" crossorigin="anonymous"></script>
    <script defer src="https://use.fontawesome.com/releases/v5.0.13/js/fontawesome.js" integrity="sha384-6OIrr52G08NpOFSZdxxz1xdNSndlD4vdcf/q2myIUVO0VsqaGHJsB0RaBE01VTOY" crossorigin="anonymous"></script>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/maps.css') }}" rel="stylesheet">
</head>
<body>
<div id="fb-root"></div>
    <!-- <script async defer crossorigin="anonymous" src="https://connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v5.0&appId=457044831662635"></script> -->

    <script>
        window.fbAsyncInit = function() {
            FB.init({
            appId      : '457044831662635',
            cookie     : true,
            xfbml      : true,
            version    : 'v5.0'
            });
            
            FB.AppEvents.logPageView();   
            
        };

        (function(d, s, id){
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) {return;}
            js = d.createElement(s); js.id = id;
            js.src = "https://connect.facebook.net/en_US/sdk.js";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    </script>

    <script>
        function usuarioConectado() {
            FB.api('/me', function(response) {
                $('#infos').html(response.name);
                $('#perfil').html('img src="https://graph.facebook.com/'+response.username+'/picture" alt="'+response.name+'" />')
            });
        }
    </script>

    <script>
        function statusChangeCallback(response) {
            if(response.status === 'connected') {
                usuarioConectado();
            }
        }
    </script>

    <script>
        FB.getLoginStatus(function(response) {
            statusChangeCallback(response);
        });    
    </script>

    <script>
        function checkLoginState() {
            FB.getLoginStatus(function(response) {
                statusChangeCallback(response);
            });
        }
    </script>

    <div id="app">
        <nav class="navbar navbar-expand-md navbar-light shadow-sm">
            <div class="container" id="navContainer">
                <button type="button" id="sidebarCollapse" class="btn btn-info">
                    <i class="fas fa-align-left"></i>
                </button>
                <a href="/home"> <img src = "{{ asset('img/bomb.png') }}" class="img-fluid" id="logo"/></a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">

                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                            <li class="nav-item">
                                <div class="fb-login-button" data-width="106" data-size="small" data-button-type="login_with" data-auto-logout-link="true" data-use-continue-as="true"></div>
                                <!-- <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a> -->
                            </li>
                            <!-- @if (Route::has('register'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('register') }}">{{ __('Cadastro') }}</a>
                                </li>
                            @endif -->
                        @else
                            <li class="nav-item dropdown">
                                <div class="fb-login-button" data-width="106" data-size="small" data-button-type="login_with" data-auto-logout-link="true" data-use-continue-as="true"></div>
                                <!-- <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a> -->

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>

        <div class="wrapper">
            <nav id="sidebar" class="sidebar2">
                <div class="sidebar-header" id="sdh"></div>
                <ul class="list-group components" id="menu">
                    <li id="add">
                        <a href="/event/create">Novo Evento</a>
                        <i class="fas fa-plus"></i>
                    </li>
                    <li>
                    </li>
                    <li id="out">
                        <a href="#" id="logout">Logout</a>
                        <i class="fas fa-sign-out-alt"></i>
                    </li>
                </ul>
            </nav>

            <!-- Sidebar -->
            <nav id="sidebar" class="sidebar">
                <div class="sidebar-header">
                                    <form method="POST" action="{{ route('event.search') }}">
                                                                                    @csrf
                                        <div class="p-1 bg-light rounded rounded-pill shadow-sm mb-4">

                                            <div class="input-group">
                                            <div class="input-group-prepend">
                                                <button id="button-addon2" type="submit" class="btn btn-link text-warning"><i class="fa fa-search"></i></button>
                                            </div>
                                            <input name="search" type="search" placeholder="" aria-describedby="button-addon2" class="form-control border-0 bg-light">
                                            </div>
                        </form>
                    </div>
                </div>

                @if(!isset($event))
                    <ul class="list-group components">
                    @if (isset($markers) && !is_null($markers))
                    @foreach ($markers as $marker)
                        <li class="list-group-item" id="lugares">
                            <a href="{{ route('event.details', $marker['id'])}}" id="nomeEstEvnt">{{$marker['name']}}</a>
                            <a id="checkin" href="{{ route('event.show', $marker['id'])}}">CheckIn</a>
                            <div class="stars">
                                @for($i = 0; $i < $marker['value']%5; $i++)
                                    <i class="fas fa-star"></i>
                                @endfor
                            </div>
                        </li>
                    @endforeach
                    @endif
                    </ul>
                @else         
                <ul class="list-group components" id="selected">
                    <li class="list-group-item">
                        <h3 href="#">{{$event['name']}}</h3>
                        <img src = "{{ asset('img/bomb.png') }}" class="img-thumbnail" id="logo"/>
                    </li>  
                    <li class="list-group-item">              
                        <div class="stars">
                            <i class="fas fa-star"></i>
                        </div>
                        <h4 id="checkin" href="{{ route('event.show', $event['id'])}}">CheckIn</h4>
                    </li>
                    <li class="list-group-item">
                        <a id="bold" href="#">{{$event['address']}}</a>
                    </li>
                    <li class="list-group-item horario">              
                        <a id="bold" href="#">Horário de Funcionamento:</a>
                        <a id="bold" href="#">{{$event['start']}} - {{$event['end']}}</a>
                    </li>
                </ul>
   
                
                
                <ul class="list-group components" id="comments">
                    <li class="list-group-item">
                        <a id="bold" href="#">@if($event->comments == null) 0 @else {{$event->comments->count()}} @endif Comentário(s)</a>
                        <div class="form-group" id="textarea">
                        <form method="POST" action="{{ route('comment.store') }}">
                                                                                    @csrf
                            <input type="text" name="comment" class="form-control z-depth-1" id="comment" rows="2" placeholder="Adicionar comentário...">
                            <input type="hidden" name="name" id="name" value="{{ Auth::user()->name }}" />
                            <input type="hidden" name="event" id="event" value="{{$event['id']}}" />

                            <button type="submit" class="btn btn-primary" id="comentar">
                                    {{ __('Comentar') }}
                                </button>
                                </form>
                        </div>
                    </li>  
                    @foreach($event->comments as $comment)
                        <li class="list-group-item">              
                        <div class="userComment">
                            <img src = "{{ asset('img/bomb.png') }}" class="img-thumbnail" id="user"/>
                            <a id="bold" href="#">{{$comment['name']}}</a>
                        </div>
                        <div class="textComment">
                            <a>{{$comment['comentario']}}</a>
                        </div>
                    </li>
                    @endforeach
                </ul>
                @endif

            </nav>

            <main class="py-4 all">
                @yield('content')
            </main>
        </div>
    </div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <!-- <script src= "https://maxcdn.Bootstrapcdn.com/Bootstrap/3.3.2/js/Bootstrap.min.js" ></script> -->

    <!-- jQuery CDN - Slim version (=without AJAX) -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <!-- Popper.JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js" integrity="sha384-cs/chFZiN24E4KMATLdqdvsezGxaGsi4hLGOzlXwp5UZB1LY//20VyM2taTB4QvJ" crossorigin="anonymous"></script>
    <!-- Bootstrap JS -->
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js" integrity="sha384-uefMccjFJAIv6A+rW+L4AHf99KvxDjWSu1z9VI8SKNVmz4sk7buKt/6v9KI65qnm" crossorigin="anonymous"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            $('.sidebar2').toggleClass('active');

            $('#sidebarCollapse').on('click', function () {
                $('.sidebar2').toggleClass('active');
            });
        });
    </script>

@yield('scripts')

</body>
</html>
