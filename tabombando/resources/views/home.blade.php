@extends('layouts.app', ['event' =>  $event ?? null ])



@section('content')

<div id="address-map-container" style="width:100%;height:600px; ">
    <div style="width: 100%; height: 100%" id="address-map"></div>
</div>
@endsection

@section('scripts')
    @parent
    <script>
    var markers = {!! $markersJson !!};
    </script>
    <script async defer src="https://maps.googleapis.com/maps/api/js?key={{ env('GOOGLE_MAPS_API_KEY') }}&callback=initMap&language=pt&region=BR"></script>

    <script src="/js/initMap.js"></script>
@stop