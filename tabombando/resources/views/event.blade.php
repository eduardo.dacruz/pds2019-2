@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">{{ __('Cadastro de Evento') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('event.store') }}">
                        @csrf

                        <div class="form-group row">
                            <div class="col-md-12">
                                <input id="nameEvent" type="text" class="form-control @error('nameEvent') is-invalid @enderror" name="nameEvent" value="{{ old('nameEvent') }}" required autocomplete="nameEvent" autofocus placeholder="Nome do Evento">

                                @error('nameEvent')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-6">
                                <input id="hrBegin" type="text" class="form-control @error('hrBegin') is-invalid @enderror" name="hrBegin" value="{{ old('hrBegin') }}" required autocomplete="hrBegin" autofocus placeholder="Hora de Início do Evento">

                                @error('hrBegin')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>

                            <div class="col-md-6">
                                <input id="dtBegin" type="text" class="form-control @error('dtBegin') is-invalid @enderror" name="dtBegin" value="{{ old('dtBegin') }}" required autocomplete="dtBegin" autofocus placeholder="Data de Início do Evento">

                                @error('dtBegin')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-6">
                                <input id="hrEnd" type="text" class="form-control @error('hrEnd') is-invalid @enderror" name="hrEnd" value="{{ old('hrEnd') }}" required autocomplete="hrEnd" autofocus placeholder="Hora de Término do Evento">

                                @error('hrEnd')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>

                            <div class="col-md-6">
                                <input id="dtEnd" type="text" class="form-control @error('dtEnd') is-invalid @enderror" name="dtEnd" value="{{ old('dtEnd') }}" required autocomplete="dtEnd" autofocus placeholder="Data de Término do Evento">

                                @error('dtEnd')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-12">
                                <input id="address" type="text" class="form-control map-input @error('address') is-invalid @enderror" name="address" value="{{ old('address') }}" required autocomplete="address" autofocus placeholder="Endereço do Evento">
                                <input type="hidden" name="address_latitude" id="address-latitude" value="0" />
                                <input type="hidden" name="address_longitude" id="address-longitude" value="0" />

                                @error('address')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div id="address-map-container" style="width:100%;height:400px; ">
                            <div style="width: 100%; height: 100%" id="address-map"></div>
                        </div>

                        <div class="form-group row" id="btnsCad">
                            <div class="col-md-6">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Cancelar') }}
                                </button>
                            </div>
                            <div class="col-md-6" id="btnConf">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Confirmar') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
    @parent
    <script src="https://maps.googleapis.com/maps/api/js?key={{ env('GOOGLE_MAPS_API_KEY') }}&libraries=places&callback=initialize" async defer></script>
    <script src="/js/mapInput.js"></script>
@stop