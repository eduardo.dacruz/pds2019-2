@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">{{ __('Cadastro de Estabelecimento') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('establishment.store') }}">
                        @csrf

                        <div class="form-group row">
                            <div class="col-md-12">
                                <input id="nameEst" type="text" class="form-control @error('nameEst') is-invalid @enderror" name="nameEst" value="{{ old('nameEst') }}" required autocomplete="nameEst" autofocus placeholder="Nome do Estabelecimento">

                                @error('nameEst')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-6">
                                <input id="hrOpen" type="text" class="form-control @error('hrOpen') is-invalid @enderror" name="hrOpen" value="{{ old('hrOpen') }}" required autocomplete="hrOpen" autofocus placeholder="Horário de Abertura">

                                @error('hrOpen')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>

                            <div class="col-md-6">
                                <input id="hrClose" type="text" class="form-control @error('hrClose') is-invalid @enderror" name="hrClose" value="{{ old('hrClose') }}" required autocomplete="hrClose" autofocus placeholder="Horário de Encerramento">

                                @error('hrClose')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-12">
                                <input id="address" type="text" class="form-control map-input @error('address') is-invalid @enderror" name="address" value="{{ old('address') }}" required autocomplete="address" autofocus placeholder="Endereço do Evento">
                                <input type="hidden" name="address_latitude" id="address-latitude" value="0" />
                                <input type="hidden" name="address_longitude" id="address-longitude" value="0" />

                                @error('address')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div id="address-map-container" style="width:100%;height:400px; ">
    <div style="width: 100%; height: 100%" id="address-map"></div>
</div>

                        <div class="form-group row" id="btnsCad">
                            <div class="col-md-6">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Cancelar') }}
                                </button>
                            </div>
                            <div class="col-md-6" id="btnConf">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Confirmar') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
    @parent
    <script src="https://maps.googleapis.com/maps/api/js?key={{ env('GOOGLE_MAPS_API_KEY') }}&libraries=places&callback=initialize" async defer></script>
    <script src="/js/mapInput.js"></script>
@stop